package fr.templin.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.templin.api.IntegrationTest;
import fr.templin.api.domain.Agenda;
import fr.templin.api.domain.enumeration.Niche;
import fr.templin.api.domain.enumeration.StatusRdv;
import fr.templin.api.repository.AgendaRepository;
import fr.templin.api.repository.search.AgendaSearchRepository;
import fr.templin.api.service.dto.AgendaDTO;
import fr.templin.api.service.mapper.AgendaMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AgendaResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class AgendaResourceIT {

    private static final LocalDate DEFAULT_DATE_RDV = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_RDV = LocalDate.now(ZoneId.systemDefault());

    private static final Niche DEFAULT_CRENEAU = Niche.MATIN;
    private static final Niche UPDATED_CRENEAU = Niche.MIDI;

    private static final StatusRdv DEFAULT_STATUS_RDV = StatusRdv.CANCEL;
    private static final StatusRdv UPDATED_STATUS_RDV = StatusRdv.DELAYD;

    private static final String ENTITY_API_URL = "/api/agenda";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/agenda";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private AgendaMapper agendaMapper;

    /**
     * This repository is mocked in the fr.templin.api.repository.search test package.
     *
     * @see fr.templin.api.repository.search.AgendaSearchRepositoryMockConfiguration
     */
    @Autowired
    private AgendaSearchRepository mockAgendaSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAgendaMockMvc;

    private Agenda agenda;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agenda createEntity(EntityManager em) {
        Agenda agenda = new Agenda().dateRDV(DEFAULT_DATE_RDV).creneau(DEFAULT_CRENEAU).statusRDV(DEFAULT_STATUS_RDV);
        return agenda;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agenda createUpdatedEntity(EntityManager em) {
        Agenda agenda = new Agenda().dateRDV(UPDATED_DATE_RDV).creneau(UPDATED_CRENEAU).statusRDV(UPDATED_STATUS_RDV);
        return agenda;
    }

    @BeforeEach
    public void initTest() {
        agenda = createEntity(em);
    }

    @Test
    @Transactional
    void createAgenda() throws Exception {
        int databaseSizeBeforeCreate = agendaRepository.findAll().size();
        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);
        restAgendaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(agendaDTO)))
            .andExpect(status().isCreated());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeCreate + 1);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getDateRDV()).isEqualTo(DEFAULT_DATE_RDV);
        assertThat(testAgenda.getCreneau()).isEqualTo(DEFAULT_CRENEAU);
        assertThat(testAgenda.getStatusRDV()).isEqualTo(DEFAULT_STATUS_RDV);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(1)).save(testAgenda);
    }

    @Test
    @Transactional
    void createAgendaWithExistingId() throws Exception {
        // Create the Agenda with an existing ID
        agenda.setId(1L);
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        int databaseSizeBeforeCreate = agendaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgendaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(agendaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeCreate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void checkDateRDVIsRequired() throws Exception {
        int databaseSizeBeforeTest = agendaRepository.findAll().size();
        // set the field null
        agenda.setDateRDV(null);

        // Create the Agenda, which fails.
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        restAgendaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(agendaDTO)))
            .andExpect(status().isBadRequest());

        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllAgenda() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        // Get all the agendaList
        restAgendaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agenda.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateRDV").value(hasItem(DEFAULT_DATE_RDV.toString())))
            .andExpect(jsonPath("$.[*].creneau").value(hasItem(DEFAULT_CRENEAU.toString())))
            .andExpect(jsonPath("$.[*].statusRDV").value(hasItem(DEFAULT_STATUS_RDV.toString())));
    }

    @Test
    @Transactional
    void getAgenda() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        // Get the agenda
        restAgendaMockMvc
            .perform(get(ENTITY_API_URL_ID, agenda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agenda.getId().intValue()))
            .andExpect(jsonPath("$.dateRDV").value(DEFAULT_DATE_RDV.toString()))
            .andExpect(jsonPath("$.creneau").value(DEFAULT_CRENEAU.toString()))
            .andExpect(jsonPath("$.statusRDV").value(DEFAULT_STATUS_RDV.toString()));
    }

    @Test
    @Transactional
    void getNonExistingAgenda() throws Exception {
        // Get the agenda
        restAgendaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAgenda() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();

        // Update the agenda
        Agenda updatedAgenda = agendaRepository.findById(agenda.getId()).get();
        // Disconnect from session so that the updates on updatedAgenda are not directly saved in db
        em.detach(updatedAgenda);
        updatedAgenda.dateRDV(UPDATED_DATE_RDV).creneau(UPDATED_CRENEAU).statusRDV(UPDATED_STATUS_RDV);
        AgendaDTO agendaDTO = agendaMapper.toDto(updatedAgenda);

        restAgendaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, agendaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isOk());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getDateRDV()).isEqualTo(UPDATED_DATE_RDV);
        assertThat(testAgenda.getCreneau()).isEqualTo(UPDATED_CRENEAU);
        assertThat(testAgenda.getStatusRDV()).isEqualTo(UPDATED_STATUS_RDV);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository).save(testAgenda);
    }

    @Test
    @Transactional
    void putNonExistingAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, agendaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void putWithIdMismatchAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(agendaDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void partialUpdateAgendaWithPatch() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();

        // Update the agenda using partial update
        Agenda partialUpdatedAgenda = new Agenda();
        partialUpdatedAgenda.setId(agenda.getId());

        partialUpdatedAgenda.dateRDV(UPDATED_DATE_RDV);

        restAgendaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAgenda.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAgenda))
            )
            .andExpect(status().isOk());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getDateRDV()).isEqualTo(UPDATED_DATE_RDV);
        assertThat(testAgenda.getCreneau()).isEqualTo(DEFAULT_CRENEAU);
        assertThat(testAgenda.getStatusRDV()).isEqualTo(DEFAULT_STATUS_RDV);
    }

    @Test
    @Transactional
    void fullUpdateAgendaWithPatch() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();

        // Update the agenda using partial update
        Agenda partialUpdatedAgenda = new Agenda();
        partialUpdatedAgenda.setId(agenda.getId());

        partialUpdatedAgenda.dateRDV(UPDATED_DATE_RDV).creneau(UPDATED_CRENEAU).statusRDV(UPDATED_STATUS_RDV);

        restAgendaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAgenda.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAgenda))
            )
            .andExpect(status().isOk());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);
        Agenda testAgenda = agendaList.get(agendaList.size() - 1);
        assertThat(testAgenda.getDateRDV()).isEqualTo(UPDATED_DATE_RDV);
        assertThat(testAgenda.getCreneau()).isEqualTo(UPDATED_CRENEAU);
        assertThat(testAgenda.getStatusRDV()).isEqualTo(UPDATED_STATUS_RDV);
    }

    @Test
    @Transactional
    void patchNonExistingAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, agendaDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAgenda() throws Exception {
        int databaseSizeBeforeUpdate = agendaRepository.findAll().size();
        agenda.setId(count.incrementAndGet());

        // Create the Agenda
        AgendaDTO agendaDTO = agendaMapper.toDto(agenda);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAgendaMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(agendaDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Agenda in the database
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(0)).save(agenda);
    }

    @Test
    @Transactional
    void deleteAgenda() throws Exception {
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);

        int databaseSizeBeforeDelete = agendaRepository.findAll().size();

        // Delete the agenda
        restAgendaMockMvc
            .perform(delete(ENTITY_API_URL_ID, agenda.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Agenda> agendaList = agendaRepository.findAll();
        assertThat(agendaList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Agenda in Elasticsearch
        verify(mockAgendaSearchRepository, times(1)).deleteById(agenda.getId());
    }

    @Test
    @Transactional
    void searchAgenda() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        agendaRepository.saveAndFlush(agenda);
        when(mockAgendaSearchRepository.search(queryStringQuery("id:" + agenda.getId()))).thenReturn(Collections.singletonList(agenda));

        // Search the agenda
        restAgendaMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + agenda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agenda.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateRDV").value(hasItem(DEFAULT_DATE_RDV.toString())))
            .andExpect(jsonPath("$.[*].creneau").value(hasItem(DEFAULT_CRENEAU.toString())))
            .andExpect(jsonPath("$.[*].statusRDV").value(hasItem(DEFAULT_STATUS_RDV.toString())));
    }
}
