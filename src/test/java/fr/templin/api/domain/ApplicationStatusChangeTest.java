package fr.templin.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import fr.templin.api.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ApplicationStatusChangeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationStatusChange.class);
        ApplicationStatusChange applicationStatusChange1 = new ApplicationStatusChange();
        applicationStatusChange1.setId(1L);
        ApplicationStatusChange applicationStatusChange2 = new ApplicationStatusChange();
        applicationStatusChange2.setId(applicationStatusChange1.getId());
        assertThat(applicationStatusChange1).isEqualTo(applicationStatusChange2);
        applicationStatusChange2.setId(2L);
        assertThat(applicationStatusChange1).isNotEqualTo(applicationStatusChange2);
        applicationStatusChange1.setId(null);
        assertThat(applicationStatusChange1).isNotEqualTo(applicationStatusChange2);
    }
}
