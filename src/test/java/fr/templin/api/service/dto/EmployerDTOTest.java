package fr.templin.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.templin.api.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmployerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployerDTO.class);
        EmployerDTO employerDTO1 = new EmployerDTO();
        employerDTO1.setId(1L);
        EmployerDTO employerDTO2 = new EmployerDTO();
        assertThat(employerDTO1).isNotEqualTo(employerDTO2);
        employerDTO2.setId(employerDTO1.getId());
        assertThat(employerDTO1).isEqualTo(employerDTO2);
        employerDTO2.setId(2L);
        assertThat(employerDTO1).isNotEqualTo(employerDTO2);
        employerDTO1.setId(null);
        assertThat(employerDTO1).isNotEqualTo(employerDTO2);
    }
}
