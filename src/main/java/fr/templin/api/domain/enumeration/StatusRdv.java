package fr.templin.api.domain.enumeration;

/**
 * The StatusRdv enumeration.
 */
public enum StatusRdv {
    CANCEL,
    DELAYD,
    TERMINATED,
    RESERVED,
}
