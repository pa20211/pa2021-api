package fr.templin.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * ApplicationStatusChange entity.\n@author The ZFahraoui team.
 */
@Entity
@Table(name = "application_status_change")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "applicationstatuschange")
public class ApplicationStatusChange implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date_changed")
    private Instant dateChanged;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "applicationstatuschanges" }, allowSetters = true)
    private ApplicationStatus applicationstatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "agenda", "applicant", "offer", "applicationstatuschanges" }, allowSetters = true)
    private Application application;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationStatusChange id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getDateChanged() {
        return this.dateChanged;
    }

    public ApplicationStatusChange dateChanged(Instant dateChanged) {
        this.dateChanged = dateChanged;
        return this;
    }

    public void setDateChanged(Instant dateChanged) {
        this.dateChanged = dateChanged;
    }

    public ApplicationStatus getApplicationstatus() {
        return this.applicationstatus;
    }

    public ApplicationStatusChange applicationstatus(ApplicationStatus applicationStatus) {
        this.setApplicationstatus(applicationStatus);
        return this;
    }

    public void setApplicationstatus(ApplicationStatus applicationStatus) {
        this.applicationstatus = applicationStatus;
    }

    public Application getApplication() {
        return this.application;
    }

    public ApplicationStatusChange application(Application application) {
        this.setApplication(application);
        return this;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationStatusChange)) {
            return false;
        }
        return id != null && id.equals(((ApplicationStatusChange) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ApplicationStatusChange{" +
            "id=" + getId() +
            ", dateChanged='" + getDateChanged() + "'" +
            "}";
    }
}
