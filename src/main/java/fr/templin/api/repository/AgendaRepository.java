package fr.templin.api.repository;

import fr.templin.api.domain.Agenda;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Agenda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgendaRepository extends JpaRepository<Agenda, Long> {
    List<Agenda> findAgendaByApplicationId(Long id);
}
