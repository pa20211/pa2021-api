package fr.templin.api.repository.search;

import fr.templin.api.domain.Education;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Education} entity.
 */
public interface EducationSearchRepository extends ElasticsearchRepository<Education, Long> {}
