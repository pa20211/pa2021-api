package fr.templin.api.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@ConfigurationProperties(prefix = "queue", ignoreUnknownFields = false)
public class AwsQueueConfiguration {

    @Value("${amazon.s3.access-key:defaultValue}")
    private String accessKey;

    @Value("${amazon.s3.secret-key:defaultValue}")
    private String accessSecret;

    @Value("${amazon.sqs.url-sqs:defaultValue}")
    private String endpoint;

    @Bean
    @Primary
    public AmazonSQSAsync amazonSQS(AWSCredentialsProvider credentials) {
        return AmazonSQSAsyncClientBuilder
            .standard()
            .withCredentials(credentials)
            .withEndpointConfiguration(new EndpointConfiguration(endpoint, "us-east-2"))
            .build();
    }

    @Bean
    @Primary
    public AWSCredentialsProvider awsCredentialsProvider() {
        return new AWSCredentialsProviderChain(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, accessSecret)));
    }
}
