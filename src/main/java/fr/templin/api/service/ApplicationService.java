package fr.templin.api.service;

import fr.templin.api.service.dto.ApplicationDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Application}.
 */
public interface ApplicationService {
    /**
     * Save a application.
     *
     * @param applicationDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicationDTO save(ApplicationDTO applicationDTO);

    /**
     * Partially updates a application.
     *
     * @param applicationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ApplicationDTO> partialUpdate(ApplicationDTO applicationDTO);

    /**
     * Get all the applications.
     *
     * @return the list of entities.
     */
    List<ApplicationDTO> findAll();

    /**
     * Get all the applications by applicant history.
     *
     * @return the list of entities.
     */
    List<ApplicationDTO> findApplicationByApplicantId(Long id);

    /**
     * Get all the applicant by offre for employer.
     *
     * @return the list of entities.
     */
    List<ApplicationDTO> findApplicationByOfferId(Long id);

    /**
     * Get the "id" application.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicationDTO> findOne(Long id);

    /**
     * Delete the "id" application.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the application corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<ApplicationDTO> search(String query);
}
