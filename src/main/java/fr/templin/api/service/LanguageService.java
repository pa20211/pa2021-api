package fr.templin.api.service;

import fr.templin.api.service.dto.LanguageDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Language}.
 */
public interface LanguageService {
    /**
     * Save a language.
     *
     * @param languageDTO the entity to save.
     * @return the persisted entity.
     */
    LanguageDTO save(LanguageDTO languageDTO);

    /**
     * Partially updates a language.
     *
     * @param languageDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LanguageDTO> partialUpdate(LanguageDTO languageDTO);

    /**
     * Get all the languages.
     *
     * @return the list of entities.
     */
    List<LanguageDTO> findAll();

    /**
     * Get the "id" language.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LanguageDTO> findOne(Long id);

    /**
     * Delete the "id" language.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the language corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<LanguageDTO> search(String query);
}
