package fr.templin.api.service;

import fr.templin.api.service.dto.ApplicationStatusChangeDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.ApplicationStatusChange}.
 */
public interface ApplicationStatusChangeService {
    /**
     * Save a applicationStatusChange.
     *
     * @param applicationStatusChangeDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicationStatusChangeDTO save(ApplicationStatusChangeDTO applicationStatusChangeDTO);

    /**
     * Partially updates a applicationStatusChange.
     *
     * @param applicationStatusChangeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ApplicationStatusChangeDTO> partialUpdate(ApplicationStatusChangeDTO applicationStatusChangeDTO);

    /**
     * Get all the applicationStatusChanges.
     *
     * @return the list of entities.
     */
    List<ApplicationStatusChangeDTO> findAll();

    /**
     * Get the "id" applicationStatusChange.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicationStatusChangeDTO> findOne(Long id);

    /**
     * Get the "id" ApplicationStatusChange By Application_Id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    List<ApplicationStatusChangeDTO> findApplicationStatusChangeByApplication_Id(Long id);

    /**
     * Delete the "id" applicationStatusChange.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the applicationStatusChange corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<ApplicationStatusChangeDTO> search(String query);
}
