package fr.templin.api.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.templin.api.config.AwsQueueConfiguration;
import fr.templin.api.service.dto.ApplicationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SqsService extends AwsQueueConfiguration {

    private final Logger log = LoggerFactory.getLogger(SqsService.class);

    @Value("${amazon.sqs.url-sqs:defaultValue}")
    private String endpoint;

    private final AmazonSQSAsync amazonSQSAsync;

    public SqsService(AmazonSQSAsync amazonSQSAsync) {
        this.amazonSQSAsync = amazonSQSAsync;
    }

    public void sendObject(ApplicationDTO jsonInput) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            // StdDateFormat is ISO8601 since jackson 2.9
            mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            mapper.registerModule(new JavaTimeModule());
            mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
            mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            String result = mapper.writeValueAsString(jsonInput);

            amazonSQSAsync.sendMessage(new SendMessageRequest(endpoint, result));
        } catch (final AmazonClientException | JsonProcessingException ase) {
            log.error("Error Message: " + ase.getMessage());
        }
    }
}
