package fr.templin.api.service.mapper;

import fr.templin.api.domain.*;
import fr.templin.api.service.dto.OfferDTO;
import javax.persistence.MapsId;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Offer} and its DTO {@link OfferDTO}.
 */
@Mapper(componentModel = "spring", uses = { EmployerMapper.class, PositionMapper.class, CategoryMapper.class })
public interface OfferMapper extends EntityMapper<OfferDTO, Offer> {
    @Mapping(target = "employer", source = "employer")
    @Mapping(target = "position", source = "position")
    @Mapping(target = "category", source = "category")
    @MapsId
    OfferDTO toDto(Offer s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OfferDTO toDtoId(Offer offer);
}
