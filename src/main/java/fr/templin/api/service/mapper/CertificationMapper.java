package fr.templin.api.service.mapper;

import fr.templin.api.domain.*;
import fr.templin.api.service.dto.CertificationDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Certification} and its DTO {@link CertificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CertificationMapper extends EntityMapper<CertificationDTO, Certification> {
    @Named("idSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    Set<CertificationDTO> toDtoIdSet(Set<Certification> certification);
}
