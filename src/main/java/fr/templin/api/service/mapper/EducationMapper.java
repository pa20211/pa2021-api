package fr.templin.api.service.mapper;

import fr.templin.api.domain.*;
import fr.templin.api.service.dto.EducationDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Education} and its DTO {@link EducationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EducationMapper extends EntityMapper<EducationDTO, Education> {
    @Named("idSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    Set<EducationDTO> toDtoIdSet(Set<Education> education);
}
