package fr.templin.api.service.mapper;

import fr.templin.api.domain.*;
import fr.templin.api.service.dto.EmployerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Employer} and its DTO {@link EmployerDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface EmployerMapper extends EntityMapper<EmployerDTO, Employer> {
    @Mapping(target = "user", source = "user", qualifiedByName = "login")
    EmployerDTO toDto(Employer s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployerDTO toDtoId(Employer employer);
}
