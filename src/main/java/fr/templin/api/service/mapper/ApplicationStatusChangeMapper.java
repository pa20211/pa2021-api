package fr.templin.api.service.mapper;

import fr.templin.api.domain.*;
import fr.templin.api.service.dto.ApplicationStatusChangeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ApplicationStatusChange} and its DTO {@link ApplicationStatusChangeDTO}.
 */
@Mapper(componentModel = "spring", uses = { ApplicationStatusMapper.class, ApplicationMapper.class })
public interface ApplicationStatusChangeMapper extends EntityMapper<ApplicationStatusChangeDTO, ApplicationStatusChange> {
    @Mapping(target = "applicationstatus", source = "applicationstatus")
    @Mapping(target = "application", source = "application")
    ApplicationStatusChangeDTO toDto(ApplicationStatusChange s);
}
