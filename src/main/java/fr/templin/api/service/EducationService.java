package fr.templin.api.service;

import fr.templin.api.service.dto.EducationDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.templin.api.domain.Education}.
 */
public interface EducationService {
    /**
     * Save a education.
     *
     * @param educationDTO the entity to save.
     * @return the persisted entity.
     */
    EducationDTO save(EducationDTO educationDTO);

    /**
     * Partially updates a education.
     *
     * @param educationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EducationDTO> partialUpdate(EducationDTO educationDTO);

    /**
     * Get all the educations.
     *
     * @return the list of entities.
     */
    List<EducationDTO> findAll();

    /**
     * Get the "id" education.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EducationDTO> findOne(Long id);

    /**
     * Delete the "id" education.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the education corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<EducationDTO> search(String query);
}
